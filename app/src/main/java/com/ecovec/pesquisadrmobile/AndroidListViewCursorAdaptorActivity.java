package com.ecovec.pesquisadrmobile;

/**
 * Created by Rodrigo on 01/02/2015.
 */

import android.app.Activity;
import android.database.Cursor;
import android.location.Location;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FilterQueryProvider;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.ecovec.pesquisadrmobile.*;

import java.io.IOException;
import java.sql.SQLException;

public class AndroidListViewCursorAdaptorActivity extends Activity {
    private DB dbHelper;
    private SimpleCursorAdapter dataAdapter;

    @Override

    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_LEFT_ICON);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        setFeatureDrawableResource(Window.FEATURE_LEFT_ICON, R.drawable.ic_launcher);

        try {
            dbHelper = new DB(this);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //dbHelper.open();

        //Clean all data
        //dbHelper.deletarTodosEnderecos();
        //Add some data
        //dbHelper.inserirEnderecos();

        //Generate ListView from SQLite Database
        displayListView();

        this.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

    }

    private void displayListView() {

        Cursor cursor = dbHelper.buscarTodosEnderecos();

        // The desired columns to be bound
        String[] columns = new String[]{
                DB.CHAVE_LOG,
                DB.CHAVE_TIPO_LOG,
                DB.CHAVE_BAIRRO,
                DB.CHAVE_CIDADE
        };

        // the XML defined views which the data will be bound to
        int[] to = new int[]{
                R.id.logradouro,
                R.id.tipoLog,
                R.id.bairro,
                R.id.cidade,
        };
        // create the adapter using the cursor pointing to the desired data
        //as well as the layout information
        dataAdapter = new SimpleCursorAdapter(
                this, R.layout.endereco_info,
                cursor,
                columns,
                to,
                0);
        ListView listView = (ListView) findViewById(R.id.listView1);
        // Assign adapter to ListView
        listView.setAdapter(dataAdapter);
        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override

            public void onItemClick(AdapterView<?> listView, View view,
                                    int position, long id) {
                // Get the cursor, positioned to the corresponding row in the result set
                Cursor cursor = (Cursor) listView.getItemAtPosition(position);
                // Get the state's capital from this row in the database.
                String nomeLocal =
                        cursor.getString(cursor.getColumnIndexOrThrow("logradouro"));
                Toast.makeText(getApplicationContext(),
                        nomeLocal, Toast.LENGTH_SHORT).show();
            }
        });

        EditText myFilter = (EditText) findViewById(R.id.myFilter);
        myFilter.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                dataAdapter.getFilter().filter(s.toString());
            }

        });
        dataAdapter.setFilterQueryProvider(new FilterQueryProvider() {

            public Cursor runQuery(CharSequence constraint) {
                return dbHelper.buscarEnderecosPorNome(constraint.toString());
            }


        });

    }
}
