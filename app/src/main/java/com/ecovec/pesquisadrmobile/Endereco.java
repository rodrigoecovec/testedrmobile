package com.ecovec.pesquisadrmobile;

/**
 * Created by Rodrigo on 01/02/2015.
 */
public class Endereco {
    String nomeLogradouro = null;
    String name = null;
    String continent = null;
    String region = null;

    public String getCode() {
        return nomeLogradouro;
    }

    public void setCode(String code) {
        this.nomeLogradouro = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContinent() {
        return continent;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }
}
