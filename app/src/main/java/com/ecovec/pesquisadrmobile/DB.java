package com.ecovec.pesquisadrmobile;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;

public class DB extends SQLiteOpenHelper {
    public static final String KEY_ROWID = "_id";
    public static final String CHAVE_LOG = "logradouro";
    public static final String CHAVE_TIPO_LOG = "tp_logradouro";
    public static final String CHAVE_BAIRRO = "bairro";
    public static final String CHAVE_CIDADE = "cidade";

    private static String DATABASE_NAME = "BaseEnderecos";
    public final static String DATABASE_PATH = "/data/data/com.ecovec.pesquisadrmobile/databases/";
    private static final int DATABASE_VERSION = 1;
    private static final String SQLITE_TABLE = "Endereco";

    private SQLiteDatabase dataBase;
    private final Context dbContext;

    public DB(Context context) throws SQLException {
        super(context, DB.DATABASE_NAME, null, DATABASE_VERSION);
        this.dbContext = context;
        DATABASE_NAME = "BaseEnderecos";
        // checking database and open it if exists
        if (checkDataBase()) {
            openDataBase();
        } else {
            try {
                this.getReadableDatabase();
                copyDataBase();
                this.close();
                openDataBase();

            } catch (IOException e) {
                throw new Error("Error copying database");
            }
            Toast.makeText(context, "Banco de Dados Inicial Criado", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    private void copyDataBase() throws IOException {
        InputStream myInput = dbContext.getAssets().open(DATABASE_NAME);
        String outFileName = DATABASE_PATH + DATABASE_NAME;
        OutputStream myOutput = new FileOutputStream(outFileName);

        byte[] buffer = new byte[1024];
        int length;
        while ((length = myInput.read(buffer)) > 0) {
            myOutput.write(buffer, 0, length);
        }

        myOutput.flush();
        myOutput.close();
        myInput.close();
    }

    public void openDataBase() throws SQLException {
        String dbPath = DATABASE_PATH + DATABASE_NAME;
        dataBase = SQLiteDatabase.openDatabase(dbPath, null, SQLiteDatabase.OPEN_READWRITE);
    }

    private boolean checkDataBase() {
        SQLiteDatabase checkDB = null;
        boolean exist = false;
        try {
            String dbPath = DATABASE_PATH + DATABASE_NAME;
            checkDB = SQLiteDatabase.openDatabase(dbPath, null,
                    SQLiteDatabase.OPEN_READONLY);
        } catch (SQLiteException e) {
            Log.v("db log", "database does't exist");
        }

        if (checkDB != null) {
            exist = true;
            checkDB.close();
        }
        return exist;
    }

    public Cursor buscarEnderecosPorNome(String inputText) throws android.database.SQLException {
        Cursor mCursor = null;
        if (inputText == null || inputText.length() == 0) {
            mCursor = dataBase.query(SQLITE_TABLE, new String[]{KEY_ROWID,
                            CHAVE_LOG, CHAVE_TIPO_LOG, CHAVE_BAIRRO, CHAVE_CIDADE},
                    null, null, null, null, null);

        } else {
            mCursor = dataBase.query(true, SQLITE_TABLE, new String[]{KEY_ROWID,
                            CHAVE_LOG, CHAVE_TIPO_LOG, CHAVE_BAIRRO, CHAVE_CIDADE},
                    CHAVE_LOG + " like '%" + inputText + "%'", null,
                    null, null, null, null);
        }
        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;

    }

    public Cursor buscarTodosEnderecos() {

        Cursor mCursor = dataBase.query(SQLITE_TABLE, new String[]{KEY_ROWID,
                        CHAVE_LOG, CHAVE_TIPO_LOG, CHAVE_BAIRRO, CHAVE_CIDADE},
                null, null, null, null, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;

    }
}